<?php

$html = '<!DOCTYPE html>';

    $html .= '<html lang="cz">';

    $html .= '<head>';

        $html .= '<title>Kalkulačka ceny energie</title>';

        $html .= '<link rel="stylesheet" type="text/css" href="/css/style.css">';

        $html .= '<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">';

//        $html .= '<link rel="icon" type="image/x-icon" href="/img/favicon.ico">'; //TODO add favicon

        $html .= '<meta charset="UTF-8">';

        $html .= '<meta name="viewport" content="width=device-width, initial-scale=1" />';

//        $html .= '<script type="text/javascript" src="/js/script.js"></script>'; //TODO add js (improve UX?)

    $html .= '</head>';

    $html .= '<body>';

        /** No header so far */
//        include 'HeaderController.php'; //TODO header (portal type? more calculators?)

        $html .= '<div class="content main-content">';

            include_once 'ContentController.php';

            $contentController = new ContentController();

            $html .= $contentController->renderView();

        $html .= '</div>';

    $html .= '</body>';

$html .= '</html>';

echo $html;
