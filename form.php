<?php

    $form = '<div class="container">';
        $form .= '<form>';
            $form .= '<div class="form-check">';
                $form .= '<input class="form-check-input" type="radio" name="energyType" id="energyTypeElectric" value="0" checked>';
                $form .= '<label class="form-check-label" for="energyTypeElectric">';
                    $form .= 'Elektřina';
                $form .= '</label>';
            $form .= '</div>';
            $form .= '<div class="form-check">';
                $form .= '<input class="form-check-input" type="radio" name="energyType" id="energyTypeGass" value="1">';
                $form .= '<label class="form-check-label" for="energyTypeGass">';
                    $form .= 'Plyn';
                $form .= '</label>';
            $form .= '</div>';
            $form .= '<div class="form-group">';
                $form .= '<label for="yearlyConsumption">Roční spotřeba energie (MWh)</label>';
                $form .= '<input type="number" min="0" max="50" step="0.1" class="form-control" name="yearlyConsumption" id="yearlyConsumption" required>';
            $form .= '</div>';
            $form .= '<div class="form-group">';
                $form .= '<label for="pricePerMWh">Cena za MWh (Kč)</label>';
                $form .= '<input type="number" min="0" step="1" class="form-control" name="pricePerMWh" id="pricePerMWh" required>';
            $form .= '</div>';
            $form .= '<div class="form-group">';
                $form .= '<label for="monthlyFee">Měsíční poplatek (Kč)</label>';
                $form .= '<input type="number" min="0" step="1"class="form-control" name="monthlyFee" id="monthlyFee" required>';
            $form .= '</div>';

            $form .= '<button type="submit" class="btn btn-primary" name="submit" value="submit">Odeslat</button>';
            $form .= '<button type="reset" class="btn btn-secondary">Reset</button>';
        $form .= '</form>';
    $form .= '</div>';
