<?php


$output = '<div class="container">';
$output .= '<table class="table">';
    $output .= '<thead>';
        $output .= '<tr>';
            $output .= '<th scope="col">#</th>';
            $output .= '<th scope="col">Dodavatel</th>';
            $output .= '<th scope="col">Celkové náklady (Kč)</th>';
        $output .= '</tr>';
    $output .= '</thead>';
    $output .= '<tbody>';
        $i = 0;
        foreach ($data as $key => $value) {
            $i++;
            $output .= '<tr>';
            $output .= '<th scope="row">'.$i.'</th>';
            $output .= '<td>'.$key.'</td>';
            $output .= '<td>'.number_format($value['total'], 0, '', ' ').' Kč</td>';
            $output .= '</tr>';
        }
    $output .= '</tbody>';
$output .= '</table>';
$output .= '</div>';