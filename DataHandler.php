<?php

class DataHandler
{
    const ENERGY_TYPE = [
        'Elektřina',
        'Plyn',
    ];

    const CONSUMPTION_RANGE = [
        '0 - 3.9 MWh',
        '4 - 20.9 MWh',
        '21 - 50 MWh',
    ];

    const SUPPLIER = [
        'Nejlevnější energie',
        'Šetříme všem',
        'Běžný dodavatel',
    ];

    const PRICE_TYPE = [
        'perMWh',
        'fee',
    ];

    private $data;

    public function __construct()
    {
        /** @var $data array Data preloading, otherwise would be loaded from MySQL, may be placed into separated PHP file */
        $data = array(
            self::ENERGY_TYPE[0] => array(
                self::CONSUMPTION_RANGE[0] => array(
                    self::SUPPLIER[0] => array(
                        self::PRICE_TYPE[0] => 1200,
                        self::PRICE_TYPE[1] => 100,
                    ),
                    self::SUPPLIER[1] => array(
                        self::PRICE_TYPE[0] => 1300,
                        self::PRICE_TYPE[1] => 110,
                    ),
                    self::SUPPLIER[2] => array(
                        self::PRICE_TYPE[0] => 1100,
                        self::PRICE_TYPE[1] => 80,
                    ),
                ),
                self::CONSUMPTION_RANGE[1] => array(
                    self::SUPPLIER[0] => array(
                        self::PRICE_TYPE[0] => 1000,
                        self::PRICE_TYPE[1] => 80,
                    ),
                    self::SUPPLIER[1] => array(
                        self::PRICE_TYPE[0] => 900,
                        self::PRICE_TYPE[1] => 70,
                    ),
                    self::SUPPLIER[2] => array(
                        self::PRICE_TYPE[0] => 1050,
                        self::PRICE_TYPE[1] => 70,
                    ),
                ),
                self::CONSUMPTION_RANGE[2] => array(
                    self::SUPPLIER[0] => array(
                        self::PRICE_TYPE[0] => 900,
                        self::PRICE_TYPE[1] => 70,
                    ),
                    self::SUPPLIER[1] => array(
                        self::PRICE_TYPE[0] => 800,
                        self::PRICE_TYPE[1] => 60,
                    ),
                    self::SUPPLIER[2] => array(
                        self::PRICE_TYPE[0] => 900,
                        self::PRICE_TYPE[1] => 80,
                    ),
                ),
            ),
            self::ENERGY_TYPE[1] => array(
                self::CONSUMPTION_RANGE[0] => array(
                    self::SUPPLIER[0] => array(
                        self::PRICE_TYPE[0] => 900,
                        self::PRICE_TYPE[1] => 100,
                    ),
                    self::SUPPLIER[1] => array(
                        self::PRICE_TYPE[0] => 930,
                        self::PRICE_TYPE[1] => 110,
                    ),
                    self::SUPPLIER[2] => array(
                        self::PRICE_TYPE[0] => 920,
                        self::PRICE_TYPE[1] => 110,
                    ),
                ),
                self::CONSUMPTION_RANGE[1] => array(
                    self::SUPPLIER[0] => array(
                        self::PRICE_TYPE[0] => 850,
                        self::PRICE_TYPE[1] => 90,
                    ),
                    self::SUPPLIER[1] => array(
                        self::PRICE_TYPE[0] => 800,
                        self::PRICE_TYPE[1] => 80,
                    ),
                    self::SUPPLIER[2] => array(
                        self::PRICE_TYPE[0] => 800,
                        self::PRICE_TYPE[1] => 80,
                    ),
                ),
                self::CONSUMPTION_RANGE[2] => array(
                    self::SUPPLIER[0] => array(
                        self::PRICE_TYPE[0] => 750,
                        self::PRICE_TYPE[1] => 80,
                    ),
                    self::SUPPLIER[1] => array(
                        self::PRICE_TYPE[0] => 730,
                        self::PRICE_TYPE[1] => 50,
                    ),
                    self::SUPPLIER[2] => array(
                        self::PRICE_TYPE[0] => 800,
                        self::PRICE_TYPE[1] => 80,
                    )
                )
            )
        );

        $this->data = $data;
    }

    /**
     * Returns Data array based on the filter values (or all Data if filters not set)
     *
     * @param $filterEnergyType int Int value defining energy type to filter (0 => Electric, 1 => Gass)
     * @param $filterConsumptionRange int Int value defining range of user energy consumption to filter (0 => '0 - 3.9 MWh', 1 => '4 - 20.9 MWh', 2 => '21 - 50 MWh',
     * @return mixed|null Array formatted filter matching data of null if filtered value does not exist
     */
    public function getData($filterEnergyType = null, $filterConsumptionRange = null)
    {
        if ($filterEnergyType === null) {
            return $this->data;
        } else {
            if (array_key_exists($filterEnergyType, self::ENERGY_TYPE)) {
                if ($filterConsumptionRange === null) {
                    return $this->data[self::ENERGY_TYPE[$filterEnergyType]];
                } else {
                    if (array_key_exists($filterConsumptionRange, self::CONSUMPTION_RANGE)) {
                        return $this->data[self::ENERGY_TYPE[$filterEnergyType]][self::CONSUMPTION_RANGE[$filterConsumptionRange]];
                    } else {
                        return null;
                    }
                }
            } else {
                return null;
            }
        }
    }


}