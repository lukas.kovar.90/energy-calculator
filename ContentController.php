<?php

include_once 'DataHandler.php';

class ContentController
{
    private $dataHandler;

    public function __construct()
    {
        $this->dataHandler = new DataHandler();
    }

    /**
     * Returns pseudo-view of either form, or final table, if the form was submitted
     * @return string view
     */
    public function renderView()
    {
        if (isset($_GET['submit']) && !empty($_GET['submit'])) {
            if (
                isset($_GET['yearlyConsumption']) && !empty($_GET['yearlyConsumption'])
                && isset($_GET['pricePerMWh']) && !empty($_GET['pricePerMWh'])
                && isset($_GET['pricePerMWh']) && !empty($_GET['pricePerMWh'])
                && $_GET['yearlyConsumption'] > 0
                && $_GET['yearlyConsumption'] < 50
            ){
                $energyType = $_GET['energyType'];
                $yearlyConsumption = $_GET['yearlyConsumption'];
                $pricePerMWh = $_GET['pricePerMWh'];
                $monthlyFee = $_GET['monthlyFee'];

                $energyConsumptionRange = $yearlyConsumption < 4 ? 0 : ($yearlyConsumption < 21 ? 1 : 2);

                $totalCosts = $yearlyConsumption * $pricePerMWh + 12 * $monthlyFee;

                if (
                    array_key_exists($energyType, DataHandler::ENERGY_TYPE)
                    && array_key_exists($energyConsumptionRange, DataHandler::CONSUMPTION_RANGE)
                ) {
                    $data = $this->dataHandler->getData($energyType, $energyConsumptionRange);

                    foreach ($data as $key => $value) {
                        $data[$key]['total'] = $yearlyConsumption * $value['perMWh'] + 12 * $value['fee'];
                    }
                    $data['Vaše spotřeba'] = array(
                        'perMWH' => $pricePerMWh,
                        'fee' => $monthlyFee,
                        'total' => $totalCosts
                    );

                    array_multisort(array_column($data, 'total'), SORT_ASC, $data);

                    $output = '';
                    include_once 'output.php';

                    return $output;

                } else {
                    return 'ERROR'; //TODO create error view, currently should not happen
                }
            } else {
                return 'ERROR';  //TODO create error view, currently should not happen
            }
        } else {
            $form = '';
            include_once 'form.php';
            return $form;
        }

    }
}